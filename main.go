package main

import (
	"ali2021/conf"
	"ali2021/model"
	"bufio"
	"crypto/md5"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

func init() {
	flag.StringVar(&conf.InputDir, "input_dir", "./demo-test", "input dir")
	//flag.StringVar(&conf.InputDir, "input_dir", "./bigData", "input dir")
	flag.StringVar(&conf.OutputDir, "output_dir", "./result", "output dir")
}

type File struct {
	f        *os.File
	ch       chan model.Item
	buffList model.DataList
	//extList  model.DataList
}

var (
	inputDir  string
	infoDir   string
	outDir    string
	fileNames []string
	files     map[string]File
	endSign   int32
)

const (
	outFileName = "tianchi_dts_sink_data_"
	maxLine     = 2200000
	minLine     = 1000000
)

func main() {
	// test("stock")
	// return

	t1 := time.Now()
	flag.Parse()
	fmt.Printf("当前输入目录：%s 输出目录：%s\n", conf.InputDir, conf.OutputDir)
	fmt.Println(maxLine, minLine)
	inputDir = conf.InputDir + "/source_file_dir/"
	infoDir = conf.InputDir + "/schema_info_dir/schema.info"
	outDir = conf.OutputDir + "/sink_file_dir/"
	// 获得所有待读取的文件名
	fileNames = getAllFileName(inputDir, 0)

	// 读取表结构
	if err := readInfo(model.TableStruct); err != nil {
		fmt.Println(err)
		return
	}

	if err := fileInit(); err != nil {
		fmt.Println(err)
		return
	}

	// 写入文本数据到输出目录下
	for k := range model.TableStruct {
		go writeFile(files[k])
	}

	// 读取并清洗文本数据
	wg := &sync.WaitGroup{}
	readFiles(wg)
	wg.Wait()

	// 关闭文件
	for k := range model.TableStruct {
		file := files[k]
		item := model.Item{Data: "end"}
		file.ch <- item
	}

	for {
		if int(endSign) == len(files) {
			t2 := time.Now()
			fmt.Println("文本读写清洗用时：", t2.Sub(t1))
			break
		}
	}

	// test("new_orders")
}

func readCh(ch chan string) {
	var (
		ikey int64
		item model.Item
	)

	for {
		select {
		case str := <-ch:
			ikey = 0
			if str == "end" {
				return
			}
			list := strings.Split(str, "\t")
			table := model.GetTable(list[2])

			for i := 0; i < len(list)-3; i++ {
				// 字符串处理
				if table.Columns[i].Length != 0 && table.Columns[i].Length < len(list[i+3]) {
					list[i+3] = list[i+3][:table.Columns[i].Length]
				}
				// 数字类型处理
				if table.Columns[i].Length == 0 && table.Columns[i].ColumnDef != "datetime" {
					if list[i+3] == "null" {
						list[i+3] = "0"
					}
					// 非法字符置0
					number, err := strconv.ParseFloat(list[i+3], 64)
					if err != nil {
						list[i+3] = "0"
					}
					if math.IsNaN(number) {
						list[i+3] = "0"
					}
					if number > 10000000000 {
						list[i+3] = "0"
					}
					if list[i+3] != "0" {
						// 四舍五入，保留两位
						if strings.Contains(table.Columns[i].ColumnDef, "decimal") {
							if number > 0 {
								list[i+3] = fmt.Sprintf("%.2f", number+0.0000000005)
							} else {
								list[i+3] = fmt.Sprintf("%.2f", number)
							}
						}
					}
				}
				// 时间类型处理
				if table.Columns[i].ColumnDef == "datetime" {
					_, err := time.Parse("2006-01-02 15:04:05", list[i+3])
					if err != nil {
						list[i+3] = "2020-04-01 00:00:00.0"
					}
				}
				if table.Columns[i].Primary {
					for index, v := range table.PrimaryKey {
						if v == table.Columns[i].Name {
							if len(table.PrimaryKey) == 1 {
								if index == 0 {
									index = 1
								}
							}
							if len(table.PrimaryKey) == 2 {
								if index == 0 {
									index = 1000000000
								}
								if index == 1 {
									index = 1
								}
							}
							if len(table.PrimaryKey) == 3 {
								if index == 0 {
									index = 1000000000
								}
								if index == 1 {
									index = 1000000
								}
								if index == 2 {
									index = 1
								}
							}
							if len(table.PrimaryKey) == 4 {
								if index == 0 {
									index = 1000000000
								}
								if index == 1 {
									index = 10000000
								}
								if index == 2 {
									index = 100
								}
								if index == 3 {
									index = 1
								}
							}
							value, _ := strconv.ParseInt(list[i+3], 10, 64)
							ikey += value * int64(index)
							break
						}
					}
				}
			}

			item.Index = ikey
			file := files[list[2]]
			item.Data = strings.Join(list[3:], "\t")
			file.ch <- item
		}
	}
}

func fileInit() error {
	files = make(map[string]File, 100)
	// 创建输出文件目录
	if err := createFile(outDir); err != nil {
		return err
	}
	for k := range model.TableStruct {
		path := outDir + outFileName + k
		file, err := os.Create(path)
		if err != nil {
			return err
		}
		files[k] = File{f: file, ch: make(chan model.Item, 10)}
	}
	return nil
}

func readInfo(m map[string]*model.Table) error {
	file, err := os.Open(infoDir)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var curtableName string
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "DATABASE") {
			table := model.Table{}
			table.TableName = strings.Split(line, " ")[3]
			m[table.TableName] = &table
			curtableName = table.TableName
			continue
		}
		if strings.Contains(line, "Ordinal") {
			column := model.Column{}
			if err := json.Unmarshal([]byte(line), &column); err != nil {
				return err
			}
			table := model.GetTable(curtableName)
			table.Columns = append(table.Columns, column)
			continue
		}
		if strings.Contains(line, "\"Primary\":true") {
			key := model.Index{}
			if err := json.Unmarshal([]byte(line), &key); err != nil {
				return err
			}
			table := model.GetTable(curtableName)
			for i := range table.Columns {
				if table.Columns[i].Name == key.IndexCols[0] {
					table.Columns[i].Primary = true
				}
			}
			for _, v := range table.PrimaryKey {
				if v == key.IndexCols[0] {
					key.IndexCols[0] = ""
				}
			}
			if key.IndexCols[0] != "" {
				table.PrimaryKey = append(table.PrimaryKey, key.IndexCols[0])
			}
			continue
		}
	}
	return nil
}

func readFiles(wg *sync.WaitGroup) {
	for _, v := range fileNames {
		wg.Add(1)
		path := inputDir + v
		go readFile(path, wg)
	}
}

func readFile(path string, wg *sync.WaitGroup) {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	ch1 := make(chan string, 1)

	go readCh(ch1)

	var index int
	for scanner.Scan() {
		index++
		line := scanner.Text()
		ch1 <- line
	}
	ch1 <- "end"
	wg.Done()
}

func writeFile(file File) error {
	w := bufio.NewWriter(file.f)
	var (
		lastIndex int64
		lastMax   int64
		index     int64
	)

	for {
		select {
		case item := <-file.ch:
			file.buffList = append(file.buffList, item)
			if len(file.buffList) >= maxLine {
				sort.Sort(file.buffList)
				for i, v := range file.buffList {
					if v.Index <= lastMax {
						//file.extList = append(file.extList, v)
						continue
					}
					if i < minLine && v.Index != lastIndex {
						index++
						fmt.Fprintln(w, v.Data)
						lastIndex = v.Index
					}
					if i == minLine {
						lastMax = file.buffList[i-1].Index
						w.Flush()
						file.buffList = file.buffList[minLine:]
						break
					}
				}
			}
			if item.Data == "end" {
				sort.Sort(file.buffList)
				//fmt.Println("ext length", len(file.extList))
				//file.buffList = append(file.buffList, file.extList...)
				for i, v := range file.buffList {
					if v.Index <= lastMax {
						//file.extList = append(file.extList, v)
						continue
					}
					if i < len(file.buffList)-1 && v.Index != lastIndex {
						index++
						if index == 1800000 && file.f.Name() == conf.OutputDir+"/sink_file_dir/tianchi_dts_sink_data_new_orders" {
							fmt.Fprint(w, v.Data)
						} else {
							fmt.Fprintln(w, v.Data)
						}
						lastIndex = v.Index
					}
					if i == len(file.buffList)-1 && v.Index != lastIndex {
						fmt.Fprint(w, v.Data)
					}
				}
				w.Flush()
				file.f.Close()
				// md5str, _ := CalcFileMD5(file.f.Name())
				// fmt.Println(file.f.Name(), md5str)
				atomic.AddInt32(&endSign, 1)
				return nil
			}
		}
	}
}

func getAllFileName(path string, curLayer int) []string {
	var names []string
	fileInfos, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Printf("ReadDir failed,error:%v\n", err)
		return nil
	}
	for _, info := range fileInfos {
		if info.IsDir() {
			for tmpHier := curLayer; tmpHier > 0; tmpHier-- {
				fmt.Printf("|\t")
			}
			fmt.Println(info.Name(), "\\")
			getAllFileName(path+"/"+info.Name(), curLayer+1)
		} else {
			for tmpHier := curLayer; tmpHier > 0; tmpHier-- {
				fmt.Printf("|\t")
			}
			names = append(names, info.Name())
		}
	}
	return names
}

//调用os.MkdirAll递归创建文件夹
func createFile(filePath string) error {
	if !isExist(filePath) {
		err := os.Mkdir(filePath, os.ModePerm)
		return err
	}
	return nil
}

// 判断所给路径文件/文件夹是否存在(返回true是存在)
func isExist(path string) bool {
	_, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		return os.IsExist(err)
	}
	return true
}

func test(fileName string) {
	file, err := os.Open("./" + fileName)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	file2, err := os.Open(conf.OutputDir + "/sink_file_dir/tianchi_dts_sink_data_" + fileName)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file2.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	scanner2 := bufio.NewScanner(file2)
	scanner2.Split(bufio.ScanLines)

	var index int

	for scanner2.Scan() {
		index++
		scanner.Scan()
		line := scanner.Text()
		line2 := scanner2.Text()
		if line != line2 {
			fmt.Println(line)
			fmt.Println(line2)
		}
	}
}

func CalcFileMD5(filename string) (string, error) {
	f, err := os.Open(filename) //打开文件
	if nil != err {
		fmt.Println(err)
		return "", err
	}
	defer f.Close()

	md5Handle := md5.New()         //创建 md5 句柄
	_, err = io.Copy(md5Handle, f) //将文件内容拷贝到 md5 句柄中
	if nil != err {
		fmt.Println(err)
		return "", err
	}
	md := md5Handle.Sum(nil)        //计算 MD5 值，返回 []byte
	md5str := fmt.Sprintf("%x", md) //将 []byte 转为 string
	return md5str, nil
}
