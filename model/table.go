package model

import "sync"

type Table struct {
	TableName  string
	PrimaryKey []string
	Columns    []Column
	sync.Mutex
	// Idexs      []Index
	// Keys       []Index
}

type Column struct {
	Name      string
	Ordinal   int
	Unsigned  bool
	CharSet   string
	ColumnDef string
	Length    int
	Precision int
	Scale     int
	Primary   bool
}

type Index struct {
	IndexName string
	IndexCols []string
	Primary   bool
	Unique    bool
}

type TableMapStruct struct {
	Data map[string]map[string]string
}

var (
	TableStruct map[string]*Table
	TableMap    TableMapStruct
	KeyMap      map[string]struct{}
)

func init() {
	TableStruct = make(map[string]*Table, 100)
	TableMap = TableMapStruct{}
	TableMap.Data = map[string]map[string]string{}
}

func GetTable(name string) *Table {
	return TableStruct[name]
}

type Item struct {
	Index int64
	Data  string
}

type DataList []Item

//Len()
func (s DataList) Len() int {
	return len(s)
}

//Less():成绩将有低到高排序
func (s DataList) Less(i, j int) bool {
	return s[i].Index < s[j].Index
}

//Swap()
func (s DataList) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
